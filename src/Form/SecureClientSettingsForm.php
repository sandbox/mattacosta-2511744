<?php

/**
 * @file
 * Contains \Drupal\secure_client\SecureClientSettingsForm.
 */

namespace Drupal\secure_client\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure HTTPS client settings for this site.
 */
class SecureClientSettingsForm extends ConfigFormBase {
  
  /**
   * For whatever reason this constant is not defined by PHP.
   * @see https://bugs.php.net/bug.php?id=68658
   */
  const CURLE_SSL_CACERT_BADFILE = 77;
  
  /**
   * The connection test was successful.
   */
  const RESULT_CONNECTION_OK = 0;
  
  /**
   * The connection test failed for an unspecified reason.
   */
  const RESULT_UNKNOWN_ERROR = 1;
  
  /**
   * The connection test resulted in a certificate validation failure.
   */
  const RESULT_CACERT_ERROR = 2;
  
  /**
   * The HTTP client used to make a request.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;
  
  /**
   * Gets a HTTP client.
   *
   * @return \GuzzleHttp\ClientInterface
   *   A HTTP client instance.
   */
  protected function httpClient() {
    if (!$this->httpClient) {
      $this->httpClient = \Drupal::httpClient();
    }
    return $this->httpClient;
  }
  
  /**
   * Determines if a connection to a secure URL is successful.
   *
   * @param string $url
   *   The URL to test.
   *
   * @return int
   *   One of the following values:
   *   - RESULT_CONNECTION_OK: The connection test succeeded.
   *   - RESULT_UNKNOWN_ERROR: The connection failed due to an unspecified
   *     error, which may or may not include SSL validation failures.
   *   - RESULT_CACERT_ERROR: The connection failed because the peer could not
   *     be verified.
   */
  protected function testConnection($url) {
    if (parse_url($url, PHP_URL_SCHEME) !== 'https') {
      throw new \InvalidArgumentException('Invalid URL scheme.');
    }
    
    try {
      $this->httpClient()->get($url);
    }
    catch (\GuzzleHttp\Exception\ClientException $e) {
      // HTTP 4xx error; ignore.
    }
    catch (\GuzzleHttp\Exception\ServerException $e) {
      // HTTP 5xx error; ignore.
    }
    catch (\GuzzleHttp\Exception\RequestException $e) {
      // This could be caused by a wide range of issues, but there is no
      // particularly good way of narrowing it down.
      return self::RESULT_UNKNOWN_ERROR;
    }
    
    return self::RESULT_CONNECTION_OK;
  }
  
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('secure_client.settings');
    
    // It is significantly easier to just test a connection than it is to try
    // and detect invalid configuration settings across all platforms, on all
    // supported PHP versions, while accounting for vendor library behaviors
    // (much less code from contrib modules).
    $status = $this->testConnection('https://www.drupal.org/');
    if ($status == self::RESULT_CONNECTION_OK) {
      $message = t('A secure connection was successfully established. No further action is required.');
    }
    else if ($status == self::RESULT_CACERT_ERROR) {
      $message = t('Unable to verify certificate. Specifying an appropriate CA bundle should correct this issue.');
    }
    else {
      $message = t('Unable to establish connection. This may be caused by a missing CA bundle, or the network connection may be unavailable.');
    }
    
    // Make error messages a little more obvious (but not the entire form item).
    if ($status != self::RESULT_CONNECTION_OK) {
      $message = '<div class="color-error">' . $message . '</div>';
    }
    
    $form['status'] = [
      '#type' => 'item',
      '#title' => t('Connection status'),
      '#markup' => $message,
    ];
    
    $form['settings'] = [
      '#type' => 'details',
      '#title' => t('SSL context options'),
      '#open' => TRUE,
    ];
    
    $form['settings']['cafile'] = [
      '#type' => 'textfield',
      '#title' => t('CA bundle'),
      '#description' => t('The location of a file containing the public keys of trusted Certificate Authorities.<br/>' .
        'If needed, a bundle of root certificates from Mozilla is provided by the <a href="@curl-url">cURL authors</a>.',
        ['@curl-url' => 'http://curl.haxx.se/docs/caextract.html']),
      '#default_value' => $config->get('cafile'),
    ];
    
    $form['settings']['ciphers'] = [
      '#type' => 'textfield',
      '#title' => t('Ciphers'),
      '#description' => t('A list of ciphers which should or should not be used when establishing a secure connection. ' .
        'The format is described in the <a href="@cipher-url">ciphers</a> help page.<br/>' .
        'This field may be left empty, as the client\'s default cipher list will be used instead.',
        ['@cipher-url' => 'https://www.openssl.org/docs/apps/ciphers.html#CIPHER-LIST-FORMAT']),
      '#default_value' => $config->get('ciphers'),
      '#maxlength' => 768,
    ];
    
    return parent::buildForm($form, $form_state);
  }
  
  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['secure_client.settings'];
  }
  
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'secure_client_settings';
  }
  
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $cafile = trim($form_state->getValue('cafile'));
    if (!empty($cafile) && !file_exists($cafile)) {
      $form_state->setErrorByName('cafile', t('The specified CA bundle does not exist.'));
    }
  }
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('secure_client.settings')
      ->set('cafile', trim($form_state->getValue('cafile')))
      ->set('ciphers', trim($form_state->getValue('ciphers')))
      ->save();
    drupal_flush_all_caches();
    parent::submitForm($form, $form_state);
  }
  
}
