<?php

/**
 * @file
 * Contains \Drupal\secure_client\SecureClientServiceProvider.
 */

namespace Drupal\secure_client;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceModifierInterface;

/**
 * Replaces the default HTTP client, with a client that can be configured to
 * make secure connections.
 */
class SecureClientServiceProvider implements ServiceModifierInterface {
  
  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    if ($container->has('http_client')) {
      $definition = $container->getDefinition('http_client');
      $definition->setClass('Drupal\\secure_client\\SecureClient');
    }
  }
  
}
