<?php

/**
 * @file
 * Contains \Drupal\secure_client\SecureClient.
 */

namespace Drupal\secure_client;

use Drupal\Core\Http\Client;

/**
 * A configurable HTTP client that better supports secure connections.
 */
class SecureClient extends Client {
  
  /**
   * {@inheritdoc}
   */
  public function __construct(array $config = []) {
    $config = \Drupal::config('secure_client.settings');
    
    $cafile = $config->get('cafile');
    $options = ['verify' => empty($cafile) ? TRUE : $cafile];
    
    $ciphers = $config->get('ciphers');
    if (!empty($ciphers)) {
      $options['config']['stream_context']['ssl']['ciphers'] = $ciphers;
      if (extension_loaded('curl')) {
        $options['config']['curl'][CURLOPT_SSL_CIPHER_LIST] = $ciphers;
      }
    }
    
    parent::__construct(['defaults' => $options]);
  }
  
}